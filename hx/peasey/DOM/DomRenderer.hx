package peasey.dom;

import peasey.dom.Mouse;
import peasey.dom.Scene;
import peasey.base.Renderer;
import js.Browser.*;
import js.html.Node;


class DomRenderer extends Renderer{
    
    var mouse = new Mouse();
    var scene = null;
    public function new(?options:Dynamic){
        super();
    }
    

    
    public function render(scene:Scene){
        if(this.scene == null){
            this.scene = scene;
            for(i in 0...this.scene.children.length){
                var node = this.scene.children[i];
                //append all the children of the scene into the rendering area
                this.append(node);
            }
        }
        this.run();
    }

    public function append(node:DomBody){
        this.domElement.appendChild(node.domElement);
    }
    
     /**
     *  Our Animation Loop
     */ 
    public function run(){
    
        scene.update(mouse);  
        this.requestAnimationFrame(run);        
    }
    
  
}