package peasey.dom;
import peasey.base.DisplayObject;
import peasey.math.Vector;
import peasey.behaviors.Behavior;
import js.html.*;
import js.Browser.*;

class Scene{
    
    //defines the boundries of the world.
    var width:Int;
    var height:Int;
    
    //ticker to keep track of time
    var ticker:Float;
    
    var domElement:Element;
    
    //children in the scene
    public var children = [];
    
    //behaviors affecting all children in the scene
    public var behaviors = [];
    
    //gravity
    var gravity:Vector;
    
    public function new(?options:Dynamic){
        /**
         *  By default, the dimensions should match the current browser window. 
         *  and resize according as such.
         */
         //TODO implement varied boundries at a later date. 
        domElement = document.createElement("div");
        domElement.style.width = window.innerWidth + "px";
        domElement.style.height = window.innerHeight + "px";
        
        //gravity = new Vector(0.0,0.05,0.0);
    }
    
    //adds a child to the scene
    public function add(obj:DomBody){
        //TODO implement varied boundries at a later date. 
        obj.setBoundries(window.innerWidth,window.innerHeight);
        children.push(obj);
    }
    
    //applies a force to the scene's children
    public function addForce(force:Vector){
         for(i in 0...children.length){
            var child = children[i];
            child.addForce(force); 
        }
    }

    public function addBehavior(behavior:Behavior){
        behaviors.push(behavior);
    }
        
    ///updates the scene using requestAnimationFrame
    public function update(mouse:Mouse){
        for(i in 0...children.length){
            var child = children[i];
            child.updateMouse(mouse);
            child.update();
            child.updateElement();
            
        }
    }

   
    
    
}