package peasey.dom;

import peasey.base.DisplayObject;
import peasey.dom.Mouse;

import js.Browser.*;



class DomBody extends DisplayObject{

    //reference to the element
    public var domElement:Dynamic;

    //list of transforms being applied
    var transforms = [
        "translate3d(0,0,0)"
    ];

    //reference to the mouse;
    var mouse = new Mouse();

    //have the transforms been applied?
    var appliedTransforms:Bool = false;
    
    //constructor
    public function new(?options:Dynamic){
        super();


        //setup the domElement
        domElement = document.createElement("div");
        domElement.style.position = "absolute";
        applyTransforms();
        
        //apply some default styling
        applyStyles({
            height:100 + "px",
            width:100 + "px"
        });

        //loop through options and see if we need to do anything else
        for(n in Reflect.fields(options)){
            switch(n){
                    
                //you can associate a className with a dom body to absob already defined css
                case "className":
                        domElement.className = Reflect.field(options,n);
                    break;
                
                //start to apply physics calculations to something already on the Dom.
                //pass in a node object
                case "domElement":
                        domElement = Reflect.field(options,n);
                        //setup the domElement
                        domElement = document.createElement("div");
                        domElement.style.position = "absolute";
                        applyTransforms();
                    break;

                case "styles":
                        applyStyles(Reflect.field(options,n));
                    break;
            }
        }

    }
                    
    static public function create(?options:Dynamic){
        return new DomBody(options);   
    }
    
    //adds a transformation to the stack.
    public function addTransform(transform:String){
        transforms = [];
        transforms.push(transform);
        appliedTransforms = false;
        return this;
    }

    //takes the current transform stack and applies it to the element
    public function applyTransforms(?multiple:Bool=false){
        
        if(multiple == false){
          
            domElement.style.webkitTransform = transforms.join("");
            //domElement.style.transform = transforms.join("");
            domElement.style.mozTransform = transforms.join("");
        }else{
            domElement.style.webkitTransform = transforms.join(" ");
            //domElement.style.transform = transforms.join(" ");
            domElement.style.mozTransform = transforms.join(" ");
        }
         
        appliedTransforms = true;
    }
    
    //updates the reference to the global mouse object
    public function updateMouse(mouse:Mouse){
        this.mouse = mouse;
    }

    /**
     *  applies a set of styles to the element;
     */
    public function applyStyles(styles:Dynamic){
       for(n in Reflect.fields(styles)){
            domElement.style.n = Reflect.field(styles,n);
       }
    }


    //updates the body's position.
    //NOTE : Run under a loop
    public function updateElement(){
        //make sure to apply any transforms if needbe
        if(!appliedTransforms){
            applyTransforms();
        }
            
            
        update();
      
        addTransform("translate3d(" + position.x + "px," + position.y + "px,0)");
        applyTransforms();
       
    }



}
