package peasey.dom;

import peasey.math.Vector;
import js.Browser.*;
import js.html.*;    
import js.html.EventListener;

class Mouse{
    //various attributes that needs to be tracked with the mouse.
    public var position:Vector = new Vector(0,0,0);
    public var absolute:Vector = new Vector(0,0,0);
    public var offset:Vector = new Vector(0,0,0);
    public var scale:Vector = new Vector(1,1,0);
    
    //is the mouse down?
    var mouseIsDown:Bool = false;
    
    public function new(){
        /**========== EVENT CALLBACKS ===============*/
    
        var mouseup : Event -> Void = function(e:Dynamic){
            mouseIsDown = false;   
            document.getElementById("mousedown").innerHTML = '$mouseIsDown';
        }
        
        
        //set up mousemove callback
        var mousemove : Event -> Void = function(e:Dynamic){
            var eposition = getRelativePosition(e,document.body);
            var touches = e.changedTouches;
            if(touches){
                e.preventDefault;   
            }
            
            absolute.x = eposition.x;
            absolute.y = eposition.y;
            position.x = absolute.x * scale.x + offset.x;
            position.y = absolute.y * scale.y + offset.y;
            
            document.getElementById("mousePos").innerHTML = '$position';
            
        };
        
        //what happens on mousedown
        var mousedown : Event -> Void = function(e:Dynamic){
            
            var eposition = getRelativePosition(e,document.body);
            var touches = e.changedTouches;
            mouseIsDown = true;
            document.getElementById("mousedown").innerHTML = '$mouseIsDown';
            
            if(touches){
                e.preventDefault;   
            }
                
            
            absolute.x = eposition.x;
            absolute.y = eposition.y;
            position.x = absolute.x * scale.x + offset.x;
            position.y = absolute.y * scale.y + offset.y;
            
        }
        
            var keydown : Event -> Void = function(e:Dynamic){
                getRelativePosition(e,document.body);
            }
        
        window.addEventListener("keydown",keydown);
        window.addEventListener("mousemove",mousemove);
        window.addEventListener("mousedown",mousedown);
        window.addEventListener("mouseup",mouseup);
    }
    
    
    public function getRelativePosition(event:Dynamic,element:Dynamic){
        var x:Float = 0;
        var y:Float = 0;
        var elementBounds = element.getBoundingClientRect(); 
        var rootNode = document.documentElement;
        var scrollX;
        if (window.pageXOffset != null){
            scrollX = window.pageXOffset;
        }else{
            scrollX = rootNode.scrollLeft;
        }
            
            //scrollY = (window.pageYOffset !== undefined) ? window.pageYOffset : rootNode.scrollTop
            
        var scrollY;
        if(window.pageYOffset != null){
            scrollY = window.pageYOffset;   
        }else{
            scrollY = rootNode.scrollTop;
        }
            
            
        var touches = event.changedTouches;
        if(touches != null){
            x = touches[0].pageX - elementBounds.left - scrollX;
            y = touches[0].pageY - elementBounds.top - scrollY;   
        }else {
            x = event.pageX - elementBounds.left - scrollX;
            y = event.pageY - elementBounds.top - scrollY;
        }
            
        return {
            x: x / (element.clientWidth / element.offsetWidth), 
            y: y / (element.clientHeight / element.offsetHeight)   
        }
    }
    
 
        
  
   
}