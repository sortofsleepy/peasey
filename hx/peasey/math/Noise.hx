package peasey.math;

class Noise{
    var fade, grad, i, lerp, p;
    
    public function new(){}
    
    public function fade(t:Dynamic){
      return t * t * t * (t * (t * 6 - 15) + 10);   
    }
    
    public function lerp(t:Float,a:Float,b:Float){
          return a + t * (b - a);
    }
}