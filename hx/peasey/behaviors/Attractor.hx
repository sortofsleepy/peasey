package peasey.behaviors;
import peasey.behaviors.Behavior;

class Attractor extends Behavior{
    
    var x:Float;
    var y:Float;
    
    var ease:Float = 0.05;
    
    var toAttract = [];
    
    public function new(){}   
    
    public function addObject(obj:Dynamic){
        toAttract.push(obj);   
    }
    
    public function update(){
        for(i in 0...toAttract.length){
            var dx = x - toAttract[i].position.x;
            var dy = y - toAttract[i].position.y;
        
           
            toAttract[i].position.x += (dx * ease);
            toAttract[i].position.y += (dy * ease);
        
            return this; 
        }
        
    }
    
}
