package peasey.behaviors;

class Group extends Behavior{
    
    var members = [];
    var angle = 0.0;
    
    //amount of spacing between each member of the group
    var spacing = 10.0;
    
    //the object leading the group
    var leadObject:Dynamic;
    
    public function new(){}
    
    
    public function addItem(member:Dynamic){
        members.push(mem);   
    }
    
    public function setLeadItem(index:Int){
        leadObject = members[index];   
    }
    
    public function update(targetX:Float,targetY:Float){
        //lead object moves towards target
        var dx = targetX - leadObject.position.x;
        var dy = targetY - leadObject.position.y;
        
        leadObject.position.x += (dx * ease);
        leadObject.position.y += (dy * ease);
        
        //the rest of the children follow the lead object
        for(i in 0...members.length){
            if(members[i] != leadObject){
                var dx = targetX - members[i].position.x;
                var dy = targetY - members[i].position.y;
                
                //temp var to help determin spacing 
                var tx = dx - spacing;
                var ty = dy - spacing;
            
                members[i].position.x += (tx * ease);
                members[i].position.y += (ty * ease);
              
            }
        }
    }
    
    
}