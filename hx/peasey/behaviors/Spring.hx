package peasey.behaviors;

class Spring extends Behavior{
    var vx:Float;
    var vy:Float;
    
    var x:Float;
    var y:Float;
    
    var gravity:Float;
    var stiffness:Float;
    var damping:Float;
    
    public function new(){}
    
    public function addGravity(grav:Dynamic){
        gravity += grav;   
    }
    
    
    public function update(targetX:Float,targetY:Float){
        var forceX = (targetX - x) * stiffness;   
        var ax = forceX / mass;
        vx = damping * (vx + ax);
        x += vx;
        
        var forceY = (targetY - y) * stiffness;
        if(gravity > 0){
            forceY += gravity;
        }
        var ay = forceY / mass;
        vy = damping * (vy + ay);
        y += vy;
    }
}
