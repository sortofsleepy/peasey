
#if js
    import peasey.dom.*;
    import peasey.math.*;
#end


//this is really just so stuff can compile plus test
class Runner {
    static public function main():Void{
        
        //set up a new scene and dom based renderer
        var engine = new DomRenderer({
            container:"#SITE"   
        });
        
        var scene = new Scene();
        
        //make a new DOM based body
        var test = DomBody.create({
            className:"testbody"
        });
        
        //add it to the scene
        scene.add(test);
    
        //apply some gravity
        var force = new Vector(0.0,15,0.0);
        scene.addForce(force);
        
        //render teh scene
        engine.render(scene);
        
      
    }

}


