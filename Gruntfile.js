module.exports = function (grunt) {
    var fs = require("fs");
    var exec = require("child_process").exec;
    require("load-grunt-tasks")(grunt);

    //base directory that site goes into
    var base = __dirname + "/public/";

    //base javascript directory
    var jsdir = base + "js/";

    var command = "cd " + __dirname + "/hx && haxe -main Runner -js " + __dirname + "/public/js/App.js";
    console.log(command);

    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),

        shell: {

            target: {
                failOnError: false,
                command: command
            }
        },
        compass: {
            dist: {
                options: {
                    basePath: __dirname + "/public",
                    sassDir: "scss",
                    cssDir: "css"
                }

            }
        },
        watch: {
            options: {
                livereload: true
            },


            scripts: {
                files: ["hx/**/*.hx"],
                tasks: ["shell"]
            },

            compass: {
                files: [
                        __dirname + "/public/scss/*.scss",
                        __dirname + "/public/scss/partials/*.scss"
                ],
                tasks: ['compass:dist']
            }

        }
    })


    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');


    //builds everything
    grunt.registerTask('default', ['compass:dist']);

    grunt.registerMultiTask("haxe", "Haxe builder", function () {

        var command = "./build.sh"



        function process(err, out, stderr) {
            console.log(out);
            if (err) {
                grunt.fail.fatal(err);
            }

            if (stderr) {
                grunt.fail.fatal(stderr);
            }

            grunt.log.write("build done");
            console.log("done");
        };

        exec(command, process);
    });


}; //end gruntfile