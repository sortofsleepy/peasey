(function () { "use strict";
function $extend(from, fields) {
	function Inherit() {} Inherit.prototype = from; var proto = new Inherit();
	for (var name in fields) proto[name] = fields[name];
	if( fields.toString !== Object.prototype.toString ) proto.toString = fields.toString;
	return proto;
}
Math.__name__ = true;
var Reflect = function() { };
Reflect.__name__ = true;
Reflect.field = function(o,field) {
	try {
		return o[field];
	} catch( e ) {
		return null;
	}
};
Reflect.fields = function(o) {
	var a = [];
	if(o != null) {
		var hasOwnProperty = Object.prototype.hasOwnProperty;
		for( var f in o ) {
		if(f != "__id__" && f != "hx__closures__" && hasOwnProperty.call(o,f)) a.push(f);
		}
	}
	return a;
};
var Runner = function() { };
Runner.__name__ = true;
Runner.main = function() {
	var scene = new peasey.dom.Scene();
	var engine = new peasey.dom.DomRenderer({ container : "#SITE"});
	var test = peasey.dom.DomBody.create({ className : "testbody"});
	scene.add(test);
	var force = new peasey.math.Vector(0.0,15,0.0);
	scene.addForce(force);
	engine.render(scene);
};
var Std = function() { };
Std.__name__ = true;
Std.string = function(s) {
	return js.Boot.__string_rec(s,"");
};
var js = {};
js.Boot = function() { };
js.Boot.__name__ = true;
js.Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str = o[0] + "(";
				s += "\t";
				var _g1 = 2;
				var _g = o.length;
				while(_g1 < _g) {
					var i = _g1++;
					if(i != 2) str += "," + js.Boot.__string_rec(o[i],s); else str += js.Boot.__string_rec(o[i],s);
				}
				return str + ")";
			}
			var l = o.length;
			var i1;
			var str1 = "[";
			s += "\t";
			var _g2 = 0;
			while(_g2 < l) {
				var i2 = _g2++;
				str1 += (i2 > 0?",":"") + js.Boot.__string_rec(o[i2],s);
			}
			str1 += "]";
			return str1;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			return "???";
		}
		if(tostr != null && tostr != Object.toString) {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str2 = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) {
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str2.length != 2) str2 += ", \n";
		str2 += s + k + " : " + js.Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str2 += "\n" + s + "}";
		return str2;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
};
var peasey = {};
peasey.base = {};
peasey.base.DisplayObject = function() {
	this.boundries = { x : 0, y : 0};
	this.frictionAir = 0.01;
	this.friction = 0.1;
	this.restitution = 0;
	this.density = 0.001;
	this.sleepThreshold = 60;
	this.motion = 0;
	this.isSleeping = false;
	this.isStatic = false;
	this.angularVelocity = 0;
	this.angularSpeed = 0;
	this.speed = 0;
	this.torque = 0;
	this.force = { };
	this.mass = 10.0;
	this.acceleration = new peasey.math.Vector();
	this.velocity = new peasey.math.Vector();
	this.position = new peasey.math.Vector();
};
peasey.base.DisplayObject.__name__ = true;
peasey.base.DisplayObject.prototype = {
	addForce: function(force) {
		var copy = force.clone();
		copy.divideScalar(this.mass);
		this.acceleration.add(copy);
	}
	,update: function() {
		this.velocity.add(this.acceleration);
		this.position.add(this.velocity);
		this.checkBoundries();
		this.acceleration.reset();
	}
	,setBoundries: function(xBounds,yBounds) {
		this.boundries.x = xBounds;
		this.boundries.y = yBounds;
	}
	,checkBoundries: function() {
		if(this.position.x > this.boundries.x) {
			this.position.x = this.boundries.x;
			this.velocity.x *= -1;
		} else if(this.position.x < 0) {
			this.velocity.x *= -1;
			this.position.x = 0;
		}
		if(this.position.y > this.boundries.y) {
			this.position.y = this.boundries.y;
			this.velocity.y *= -1;
		} else if(this.position.y < 0) {
			this.velocity.y *= -1;
			this.position.y = 0;
		}
	}
};
peasey.base.Renderer = function(element) {
	if(element == null) this.domElement = window.document.body; else this.domElement = element;
};
peasey.base.Renderer.__name__ = true;
peasey.base.Renderer.prototype = {
	requestAnimationFrame: function(method) {
		var requestAnimationFrame = ($_=window,$bind($_,$_.requestAnimationFrame));
		if(requestAnimationFrame == null) requestAnimationFrame = function(method1) {
			window.setTimeout(method1,16.6666666666666679);
		};
		requestAnimationFrame(method);
	}
	,apply: function(obj,options) {
		var _g = 0;
		var _g1 = Reflect.fields(options);
		try {
			while(_g < _g1.length) {
				var n = _g1[_g];
				++_g;
				switch(n) {
				case "container":
					this.domElement = window.document.querySelector(Reflect.field(options,n));
					throw "__break__";
					break;
				}
			}
		} catch( e ) { if( e != "__break__" ) throw e; }
	}
};
peasey.behaviors = {};
peasey.behaviors.Behavior = function() {
};
peasey.behaviors.Behavior.__name__ = true;
peasey.dom = {};
peasey.dom.DomBody = function(options) {
	this.appliedTransforms = false;
	this.mouse = new peasey.dom.Mouse();
	this.transforms = ["translate3d(0,0,0)"];
	peasey.base.DisplayObject.call(this);
	this.domElement = window.document.createElement("div");
	this.domElement.style.position = "absolute";
	this.applyTransforms();
	this.applyStyles({ height : 100 + "px", width : 100 + "px"});
	var _g = 0;
	var _g1 = Reflect.fields(options);
	try {
		while(_g < _g1.length) {
			var n = _g1[_g];
			++_g;
			switch(n) {
			case "className":
				this.domElement.className = Reflect.field(options,n);
				throw "__break__";
				break;
			case "domElement":
				this.domElement = Reflect.field(options,n);
				this.domElement = window.document.createElement("div");
				this.domElement.style.position = "absolute";
				this.applyTransforms();
				throw "__break__";
				break;
			case "styles":
				this.applyStyles(Reflect.field(options,n));
				throw "__break__";
				break;
			}
		}
	} catch( e ) { if( e != "__break__" ) throw e; }
};
peasey.dom.DomBody.__name__ = true;
peasey.dom.DomBody.create = function(options) {
	return new peasey.dom.DomBody(options);
};
peasey.dom.DomBody.__super__ = peasey.base.DisplayObject;
peasey.dom.DomBody.prototype = $extend(peasey.base.DisplayObject.prototype,{
	addTransform: function(transform) {
		this.transforms = [];
		this.transforms.push(transform);
		this.appliedTransforms = false;
		return this;
	}
	,applyTransforms: function(multiple) {
		if(multiple == null) multiple = false;
		if(multiple == false) {
			this.domElement.style.webkitTransform = this.transforms.join("");
			this.domElement.style.mozTransform = this.transforms.join("");
		} else {
			this.domElement.style.webkitTransform = this.transforms.join(" ");
			this.domElement.style.mozTransform = this.transforms.join(" ");
		}
		this.appliedTransforms = true;
	}
	,updateMouse: function(mouse) {
		this.mouse = mouse;
	}
	,applyStyles: function(styles) {
		var _g = 0;
		var _g1 = Reflect.fields(styles);
		while(_g < _g1.length) {
			var n = _g1[_g];
			++_g;
			this.domElement.style.n = Reflect.field(styles,n);
		}
	}
	,updateElement: function() {
		if(!this.appliedTransforms) this.applyTransforms();
		this.update();
		this.addTransform("translate3d(" + this.position.x + "px," + this.position.y + "px,0)");
		this.applyTransforms();
	}
});
peasey.dom.DomRenderer = function(options) {
	this.scene = null;
	this.mouse = new peasey.dom.Mouse();
	peasey.base.Renderer.call(this);
};
peasey.dom.DomRenderer.__name__ = true;
peasey.dom.DomRenderer.__super__ = peasey.base.Renderer;
peasey.dom.DomRenderer.prototype = $extend(peasey.base.Renderer.prototype,{
	render: function(scene) {
		if(this.scene == null) {
			this.scene = scene;
			var _g1 = 0;
			var _g = this.scene.children.length;
			while(_g1 < _g) {
				var i = _g1++;
				var node = this.scene.children[i];
				this.append(node);
			}
		}
		this.run();
	}
	,append: function(node) {
		this.domElement.appendChild(node.domElement);
	}
	,run: function() {
		this.scene.update(this.mouse);
		this.requestAnimationFrame($bind(this,this.run));
	}
});
peasey.dom.Mouse = function() {
	this.mouseIsDown = false;
	this.scale = new peasey.math.Vector(1,1,0);
	this.offset = new peasey.math.Vector(0,0,0);
	this.absolute = new peasey.math.Vector(0,0,0);
	this.position = new peasey.math.Vector(0,0,0);
	var _g = this;
	var mouseup = function(e) {
		_g.mouseIsDown = false;
		window.document.getElementById("mousedown").innerHTML = "" + (_g.mouseIsDown == null?"null":"" + _g.mouseIsDown);
	};
	var mousemove = function(e1) {
		var eposition = _g.getRelativePosition(e1,window.document.body);
		var touches = e1.changedTouches;
		if(touches) e1.preventDefault;
		_g.absolute.x = eposition.x;
		_g.absolute.y = eposition.y;
		_g.position.x = _g.absolute.x * _g.scale.x + _g.offset.x;
		_g.position.y = _g.absolute.y * _g.scale.y + _g.offset.y;
		window.document.getElementById("mousePos").innerHTML = "" + Std.string(_g.position);
	};
	var mousedown = function(e2) {
		var eposition1 = _g.getRelativePosition(e2,window.document.body);
		var touches1 = e2.changedTouches;
		_g.mouseIsDown = true;
		window.document.getElementById("mousedown").innerHTML = "" + (_g.mouseIsDown == null?"null":"" + _g.mouseIsDown);
		if(touches1) e2.preventDefault;
		_g.absolute.x = eposition1.x;
		_g.absolute.y = eposition1.y;
		_g.position.x = _g.absolute.x * _g.scale.x + _g.offset.x;
		_g.position.y = _g.absolute.y * _g.scale.y + _g.offset.y;
	};
	var keydown = function(e3) {
		_g.getRelativePosition(e3,window.document.body);
	};
	window.addEventListener("keydown",keydown);
	window.addEventListener("mousemove",mousemove);
	window.addEventListener("mousedown",mousedown);
	window.addEventListener("mouseup",mouseup);
};
peasey.dom.Mouse.__name__ = true;
peasey.dom.Mouse.prototype = {
	getRelativePosition: function(event,element) {
		var x = 0;
		var y = 0;
		var elementBounds = element.getBoundingClientRect();
		var rootNode = window.document.documentElement;
		var scrollX;
		if(window.pageXOffset != null) scrollX = window.pageXOffset; else scrollX = rootNode.scrollLeft;
		var scrollY;
		if(window.pageYOffset != null) scrollY = window.pageYOffset; else scrollY = rootNode.scrollTop;
		var touches = event.changedTouches;
		if(touches != null) {
			x = touches[0].pageX - elementBounds.left - scrollX;
			y = touches[0].pageY - elementBounds.top - scrollY;
		} else {
			x = event.pageX - elementBounds.left - scrollX;
			y = event.pageY - elementBounds.top - scrollY;
		}
		return { x : x / (element.clientWidth / element.offsetWidth), y : y / (element.clientHeight / element.offsetHeight)};
	}
};
peasey.dom.Scene = function(options) {
	this.behaviors = [];
	this.children = [];
	this.domElement = window.document.createElement("div");
	this.domElement.style.width = window.innerWidth + "px";
	this.domElement.style.height = window.innerHeight + "px";
};
peasey.dom.Scene.__name__ = true;
peasey.dom.Scene.prototype = {
	add: function(obj) {
		obj.setBoundries(window.innerWidth,window.innerHeight);
		this.children.push(obj);
	}
	,addForce: function(force) {
		var _g1 = 0;
		var _g = this.children.length;
		while(_g1 < _g) {
			var i = _g1++;
			var child = this.children[i];
			child.addForce(force);
		}
	}
	,addBehavior: function(behavior) {
		this.behaviors.push(behavior);
	}
	,update: function(mouse) {
		var _g1 = 0;
		var _g = this.children.length;
		while(_g1 < _g) {
			var i = _g1++;
			var child = this.children[i];
			child.updateMouse(mouse);
			child.update();
			child.updateElement();
		}
	}
};
peasey.math = {};
peasey.math.Vector = function(x,y,z) {
	if(z == null) z = 0;
	if(y == null) y = 0;
	if(x == null) x = 0;
	this.x = x;
	this.y = y;
	this.z = z;
};
peasey.math.Vector.__name__ = true;
peasey.math.Vector.prototype = {
	clone: function() {
		return new peasey.math.Vector(this.x,this.y,this.z);
	}
	,copy: function(vec) {
		this.x = vec.x;
		this.y = vec.y;
		this.z = vec.z;
		return this;
	}
	,add: function(vec) {
		this.x += vec.x;
		this.y += vec.y;
		this.z += vec.z;
	}
	,subtract: function(vec) {
		this.x -= vec.x;
		this.y -= vec.y;
		this.z -= vec.z;
		return this;
	}
	,multiply: function(vec) {
		this.x *= vec.x;
		this.y *= vec.y;
		this.z *= vec.z;
		return this;
	}
	,divide: function(vec) {
		this.x /= vec.x;
		this.y /= vec.y;
		this.z /= vec.z;
		return this;
	}
	,divideScalar: function(val) {
		this.x /= val;
		this.y /= val;
		this.z /= val;
	}
	,reset: function() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}
	,distance: function(vec) {
		var x = vec.x - this.x;
		var y = vec.y - this.y;
		var z = vec.z - this.z;
		return Math.sqrt(x * x + y * y + z * z);
	}
	,distanceSquared: function(vec) {
		var x = vec.x - this.x;
		var y = vec.y - this.y;
		var z = vec.z - this.z;
		return x * x + y * y + z * z;
	}
	,normalize: function() {
		var x = this.x;
		var y = this.y;
		var z = this.z;
		var len = x * x + y * y + z * z;
		if(len > 0) {
			len = 1 / Math.sqrt(len);
			this.x *= len;
			this.y *= len;
			this.z *= len;
		}
		return this;
	}
	,lerp: function(vec) {
	}
};
peasey.math._Vector = {};
peasey.math._Vector.Vec3_Impl_ = function() { };
peasey.math._Vector.Vec3_Impl_.__name__ = true;
peasey.math._Vector.Vec3_Impl_._new = function() {
	return new peasey.math.Vector();
};
var $_, $fid = 0;
function $bind(o,m) { if( m == null ) return null; if( m.__id__ == null ) m.__id__ = $fid++; var f; if( o.hx__closures__ == null ) o.hx__closures__ = {}; else f = o.hx__closures__[m.__id__]; if( f == null ) { f = function(){ return f.method.apply(f.scope, arguments); }; f.scope = o; f.method = m; o.hx__closures__[m.__id__] = f; } return f; }
Math.NaN = Number.NaN;
Math.NEGATIVE_INFINITY = Number.NEGATIVE_INFINITY;
Math.POSITIVE_INFINITY = Number.POSITIVE_INFINITY;
Math.isFinite = function(i) {
	return isFinite(i);
};
Math.isNaN = function(i1) {
	return isNaN(i1);
};
String.__name__ = true;
Array.__name__ = true;
Runner.main();
})();
